import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleSearchTest {

    @Test
    public void searchWithGoogle() {

        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Drivers\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.google.com/");

        WebElement acceptCookies = webDriver.findElement(By.id("L2AGLb"));
        acceptCookies.click();

        WebElement searchInput = webDriver.findElement(By.name("q"));
        searchInput.sendKeys("Telerik Academy Alpha");
        searchInput.submit();

        WebElement title = webDriver.findElement(By.xpath("//h3[@class='LC20lb MBeuO DKV0Md']"));

        String expectedTitle = "IT Career Start in 6 Months - Telerik Academy Alpha";
        String actualTitle = title.getText();
        Assert.assertEquals(expectedTitle, actualTitle);

        webDriver.quit();
    }
}
