import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BingSearchTest {

    @Test
    public void searchWithBing() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Drivers\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.bing.com/");

        WebElement searchInput = webDriver.findElement(By.id("sb_form_q"));
        searchInput.sendKeys("Telerik Academy Alpha");

        WebElement searchButton = webDriver.findElement(By.id("search_icon"));
        searchButton.click();

        WebElement title = webDriver.findElement(By.xpath("//h2[@class=' b_topTitle']"));

        String expectedTitle = "IT Career Start in 6 Months - Telerik Academy Alpha";
        String actualTitle = title.getText();
        Assert.assertEquals(expectedTitle, actualTitle);
        webDriver.quit();
    }
}
